export const GRIDSTACK_MARGIN = 10;
export const GRIDSTACK_CSS_HANDLE = '.grid-stack-item-handle';
export const GRIDSTACK_CELL_HEIGHT = '120px';
export const GRIDSTACK_MIN_ROW = 1;
